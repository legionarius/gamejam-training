//
// Created by bogdan on 25/02/2021.
//

#ifndef GAMEJAM_TRAINING_INVENTORY_H
#define GAMEJAM_TRAINING_INVENTORY_H

#include "InventoryItem.h"
#include <Godot.hpp>
#include <HBoxContainer.hpp>
#include <Node2D.hpp>
#include <PackedScene.hpp>
#include <Ref.hpp>
#include <ResourceLoader.hpp>
#include <SceneTree.hpp>
#include <Viewport.hpp>
#include <algorithm>
#include <vector>

namespace godot {
class Inventory : public Node2D {
	GODOT_CLASS(Inventory, Node2D);

private:
	HBoxContainer *container;
	std::vector<String> items;

public:
	static void _register_methods();
	void _init();
	void _ready();
	void _update_inventory(const String type, const int quantity);
};
} // namespace godot

#endif //GAMEJAM_TRAINING_INVENTORY_H
