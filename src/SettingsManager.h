//
// Created by bogdan on 24/02/2021.
//

#ifndef GAMEJAM_TRAINING_SETTINGSMANAGER_H
#define GAMEJAM_TRAINING_SETTINGSMANAGER_H

#include <File.hpp>
#include <Godot.hpp>
#include <InputEventKey.hpp>
#include <InputMap.hpp>
#include <JSON.hpp>
#include <JSONParseResult.hpp>
#include <Node.hpp>
#include <Ref.hpp>
#include <map>

namespace godot {

class SettingsManager : public Node {
	GODOT_CLASS(SettingsManager, Node);

private:
	std::map<String, uint64_t> inputs;
	void load_inputs();

public:
	static void _register_methods();
	void _init();
	std::map<String, uint64_t> get_settings_inputs();
	void refresh_inputs();
	void update_key_event(String action, uint64_t keycode);
};
} // namespace godot

#endif //GAMEJAM_TRAINING_SETTINGSMANAGER_H
