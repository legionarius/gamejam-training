#ifndef INPUTSETTINGS_H
#define INPUTSETTINGS_H

#include "InputItem.h"
#include "SettingsManager.h"
#include <Control.hpp>
#include <File.hpp>
#include <Godot.hpp>
#include <JSON.hpp>
#include <JSONParseResult.hpp>
#include <PackedScene.hpp>
#include <Resource.hpp>
#include <ResourceLoader.hpp>
#include <SceneTree.hpp>
#include <VBoxContainer.hpp>
#include <vector>

namespace godot {

class InputSettings : public VBoxContainer {
	GODOT_CLASS(InputSettings, Control);

public:
	static void _register_methods();
	void _init();
	void _ready();
	InputSettings();
	~InputSettings();
};
} // namespace godot

#endif // INPUTSETTINGS_H
