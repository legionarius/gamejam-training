//
// Created by bogdan on 25/02/2021.
//

#ifndef GAMEJAM_TRAINING_INVENTORYITEM_H
#define GAMEJAM_TRAINING_INVENTORYITEM_H

#include <Godot.hpp>
#include <Label.hpp>

namespace godot {
class InventoryItem : public Label {
	GODOT_CLASS(InventoryItem, Label)

private:
	int quantity;
	String type;
	void update_label();

public:
	static void _register_methods();
	void _init();
	void _ready();
	void set_quantity(const int quantity);
	void set_type(const String type);
};
} // namespace godot

#endif //GAMEJAM_TRAINING_INVENTORYITEM_H
