//
// Created by bogdan on 24/02/2021.
//

#include "SettingsManager.h"

using namespace godot;

void SettingsManager::load_inputs() {
	Ref<File> file = File::_new();
	Error json_file = file->open("inputs.json", File::ModeFlags::READ);
	if (json_file == Error::ERR_FILE_CANT_OPEN) {
		Godot::print("Can't open file !");
	} else if (json_file == Error::ERR_FILE_CANT_READ) {
		Godot::print("Can't read file !");
	} else if (json_file == Error::ERR_FILE_NOT_FOUND) {
		Godot::print("File not found !");
	}
	Dictionary json_results =
			JSON::get_singleton()->parse(file->get_as_text())->get_result();
	Array n = json_results["inputs"];
	for (int i = 0; i < n.size(); i++) {
		Dictionary input = n[i];
		inputs[input["name"]] = input["keycode"];
	}
}

void SettingsManager::refresh_inputs() {
	for (auto input : inputs) {
		update_key_event(input.first, input.second);
	}
}

void SettingsManager::update_key_event(String action, uint64_t keycode) {
	Ref<InputEventKey> key;
	key.instance();
	key->set_scancode(keycode);
	if (InputMap::get_singleton()->has_action(action)) {
		InputMap::get_singleton()->action_erase_events(action);
		InputMap::get_singleton()->action_add_event(action, key);
	} else {
		InputMap::get_singleton()->add_action(action);
		InputMap::get_singleton()->action_add_event(action, key);
	}
	inputs[action] = keycode;
}

void SettingsManager::_init() {
	Godot::print("Settings Manager Initialized.");
	load_inputs();
	refresh_inputs();
}

std::map<String, uint64_t> SettingsManager::get_settings_inputs() {
	return inputs;
}

void SettingsManager::_register_methods() {
	register_method("_init", &SettingsManager::_init);
}
