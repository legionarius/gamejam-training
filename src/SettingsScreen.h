//
// Created by bogdan on 24/02/2021.
//

#ifndef GAMEJAM_TRAINING_SETTINGSSCREEN_H
#define GAMEJAM_TRAINING_SETTINGSSCREEN_H

#include <ConfigFile.hpp>
#include <Control.hpp>
#include <Godot.hpp>
#include <InputEventKey.hpp>
#include <InputMap.hpp>
#include <Ref.hpp>
#include <SceneTree.hpp>

namespace godot {

class SettingsScreen : public Control {
	GODOT_CLASS(SettingsScreen, Control);

public:
	static void _register_methods();
	void _init();
	void _ready();
	void _input(const Ref<InputEvent> event);
};

} // namespace godot

#endif //GAMEJAM_TRAINING_SETTINGSSCREEN_H
