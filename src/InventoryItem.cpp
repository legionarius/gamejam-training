//
// Created by bogdan on 25/02/2021.
//

#include "InventoryItem.h"

using namespace godot;

void InventoryItem::_ready() {
	update_label();
}

void InventoryItem::_init() {
	quantity = 0;
	type = String("Name");
}

void InventoryItem::set_quantity(const int quantity) {
	this->quantity = quantity;
	update_label();
}

void InventoryItem::set_type(const String type) {
	this->type = type;
}

void InventoryItem::update_label() {
	set_text(String(std::to_string(quantity).c_str()) + String(" ") + type.capitalize());
}

void InventoryItem::_register_methods() {
	register_method("_init", &InventoryItem::_init);
	register_method("_ready", &InventoryItem::_ready);
	register_method("set_quantity", &InventoryItem::set_quantity);
}