//
// Created by bogdan on 25/02/2021.
//

#include "Inventory.h"

using namespace godot;

void Inventory::_init() {
	items = { "pizza", "pasta", "mama" };
}

void Inventory::_ready() {
	get_tree()->get_root()->get_node("GameState")->connect("_inventory_changed", this, "_update_inventory");
	container = Object::cast_to<HBoxContainer>(get_node("ItemContainer"));
	Ref<PackedScene> inventoryItemRef = ResourceLoader::get_singleton()->load("entity/Inventory/InventoryItem.tscn");
	for (auto item : items) {
		InventoryItem *inventoryItem = Object::cast_to<InventoryItem>(inventoryItemRef->instance());
		inventoryItem->set_type(item);
		container->add_child(inventoryItem);
	}
}

void Inventory::_update_inventory(const String type, const int quantity) {
	auto it = std::find(items.begin(), items.end(), type);
	if (it != items.end()) {
		Godot::print(std::to_string(container->get_child_count()).c_str());
		InventoryItem *inventoryItem = Object::cast_to<InventoryItem>(container->get_child(0));
		inventoryItem->set_quantity(quantity);
	} else {
		Godot::print("Can't find " + type + " in Inventory !");
	}
}

void Inventory::_register_methods() {
	register_method("_init", &Inventory::_init);
	register_method("_ready", &Inventory::_ready);
	register_method("_update_inventory", &Inventory::_update_inventory);
}