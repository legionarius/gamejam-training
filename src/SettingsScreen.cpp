//
// Created by bogdan on 24/02/2021.
//

#include "SettingsScreen.h"

using namespace godot;

void SettingsScreen::_ready() {
}

void SettingsScreen::_init() {
}

void SettingsScreen::_input(const Ref<InputEvent> event) {
	Ref<InputEventKey> event_key = event;
	// Escape code taken from keyboard.h in Godot
	// TODO : include file in project
	if (event_key.is_valid() && event_key->is_pressed() && event_key->get_scancode() == ((1 << 24) | 0x01)) {
		get_tree()->change_scene("entity/TitleScreen/TitleScreen.tscn");
	}
}

void SettingsScreen::_register_methods() {
	register_method("_init", &SettingsScreen::_init);
	register_method("_ready", &SettingsScreen::_ready);
	register_method("_input", &SettingsScreen::_input);
}
