#include "InputSettings.h"

using namespace godot;

InputSettings::InputSettings() {}

InputSettings::~InputSettings() {}

void InputSettings::_ready() {
	Ref<PackedScene> inputItem = ResourceLoader::get_singleton()->load(
			"entity/SettingsScreen/InputItem.tscn");
	SettingsManager *settingsManager = Object::cast_to<SettingsManager>(get_tree()->get_root()->get_node("SettingsManager"));
	auto inputs = settingsManager->get_settings_inputs();
	for (auto item : inputs) {
		InputItem *instance = Object::cast_to<InputItem>(inputItem->instance());
		if (instance == nullptr) {
			Godot::print("Cast failed !");
		} else {
			instance->set_action(item.first);
			instance->set_keycode(item.second);
			add_child(instance);
		}
	}
}

void InputSettings::_init() {
}

void InputSettings::_register_methods() {
	register_method("_init", &InputSettings::_init);
	register_method("_ready", &InputSettings::_ready);
}
