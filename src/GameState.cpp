//
// Created by bogdan on 25/02/2021.
//

#include "GameState.h"

using namespace godot;

void GameState::start_game() {
	set_initial_state();
	get_tree()->change_scene("entity/MainScene/MainScene.tscn");
	isStarted = true;
}

void GameState::set_initial_state() {
}

void GameState::go_to_menu() {
	get_tree()->change_scene("entity/TitleScreen/TitleScreen.tscn");
}

void GameState::_init() {
	isStarted = false;
}

void GameState::_input(const Ref<InputEvent> event) {
	if (isStarted) {
		Ref<InputEventKey> event_key = event;

		if (event_key.is_valid() && event_key->is_pressed() && event_key->get_scancode() == ((1 << 24) | 0x01)) {
			// TODO : Pause Game
			// Go to Main Menu OR Show pause menu
			go_to_menu();
		}

		if (InputMap::get_singleton()->event_is_action(event, "Up")) {
			emit_signal("_inventory_changed", String("pizza"), 2);
		}
	}
}

void GameState::_register_methods() {
	register_method("_init", &GameState::_init);
	register_method("_input", &GameState::_input);
	register_signal<GameState>("_inventory_changed", "type", GODOT_VARIANT_TYPE_STRING, "quantity", GODOT_VARIANT_TYPE_INT);
}