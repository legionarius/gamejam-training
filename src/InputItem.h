#ifndef INPUTITEM_H
#define INPUTITEM_H

#include "SettingsManager.h"
#include <Button.hpp>
#include <ConfirmationDialog.hpp>
#include <Godot.hpp>
#include <GodotGlobal.hpp>
#include <HBoxContainer.hpp>
#include <InputEvent.hpp>
#include <InputEventKey.hpp>
#include <InputMap.hpp>
#include <Label.hpp>
#include <OS.hpp>
#include <Ref.hpp>
#include <SceneTree.hpp>
#include <Viewport.hpp>
#include <VisibilityNotifier.hpp>

namespace godot {
class InputItem : public HBoxContainer {
	GODOT_CLASS(InputItem, HBoxContainer);

private:
	Button *button;
	Label *label;
	bool wait_for_input;
	int64_t keycode;
	String action;
	void _on_button_pressed();
	void _update_key_event(const String action, const int64_t keycode);

public:
	static void _register_methods();
	void _init();
	void _ready();
	void _input(const Ref<InputEvent> event);
	int get_keycode();
	void set_keycode(const int64_t code);
	String get_action();
	void set_action(const String action);
};
} // namespace godot

#endif // INPUTITEM_H
