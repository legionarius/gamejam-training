//
// Created by bogdan on 24/02/2021.
//

#include "TitleScreen.h"

using namespace godot;

void TitleScreen::_init() {
}

void TitleScreen::_ready() {
	Button *exitBtn = Object::cast_to<Button>(get_node("MainMenu/VBoxContainer/Exit"));
	Button *startBtn = Object::cast_to<Button>(get_node("MainMenu/VBoxContainer/Start"));
	Button *settingsBtn = Object::cast_to<Button>(get_node("MainMenu/VBoxContainer/Settings"));
	exitBtn->connect("pressed", this, "_exit");
	startBtn->connect("pressed", this, "_start");
	settingsBtn->connect("pressed", this, "_navigate_to_settings");
}

void TitleScreen::_navigate_to_settings() {
	get_tree()->change_scene("entity/SettingsScreen/SettingsScreen.tscn");
}

void TitleScreen::_exit() {
	get_tree()->quit();
}

void TitleScreen::_start() {
	GameState *gameState = Object::cast_to<GameState>(get_tree()->get_root()->get_node("GameState"));
	if (gameState == nullptr) {
		Godot::print("POinter NULLLLLL");
	}
	gameState->start_game();
}

void TitleScreen::_input(const Ref<InputEvent> event) {
	Ref<InputEventKey> event_key = event;
	// Escape code taken from keyboard.h in Godot
	// TODO : include file in project
	if (event_key.is_valid() && event_key->is_pressed() && event_key->get_scancode() == ((1 << 24) | 0x01)) {
		_exit();
	}

	if (InputMap::get_singleton()->event_is_action(event, "Up")) {
		Godot::print("Jump was pressed");
	}
}

void TitleScreen::_register_methods() {
	register_method("_init", &TitleScreen::_init);
	register_method("_ready", &TitleScreen::_ready);
	register_method("_exit", &TitleScreen::_exit);
	register_method("_start", &TitleScreen::_start);
	register_method("_input", &TitleScreen::_input);
	register_method("_navigate_to_settings", &TitleScreen::_navigate_to_settings);
}
