//
// Created by bogdan on 11/02/2021.
//

#include "GameState.h"
#include "InputItem.h"
#include "InputSettings.h"
#include "Inventory.h"
#include "InventoryItem.h"
#include "SettingsManager.h"
#include "SettingsScreen.h"
#include "TitleScreen.h"

extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
	godot::Godot::gdnative_init(o);
}

extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
	godot::Godot::gdnative_terminate(o);
}

extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
	godot::Godot::nativescript_init(handle);
	godot::register_class<godot::GameState>();
	godot::register_class<godot::InputItem>();
	godot::register_class<godot::InputSettings>();
	godot::register_class<godot::SettingsManager>();
	godot::register_class<godot::SettingsScreen>();
	godot::register_class<godot::TitleScreen>();
	godot::register_class<godot::Inventory>();
	godot::register_class<godot::InventoryItem>();
}
