#include "InputItem.h"

using namespace godot;

void InputItem::_init() {
	wait_for_input = false;
}

void InputItem::_ready() {
	button = Object::cast_to<Button>(get_node("Button"));
	label = Object::cast_to<Label>(get_node("Label"));
	button->connect("pressed", this, "_on_button_pressed");
	label->set_text(action);
	String key_name = OS::get_singleton()->get_scancode_string(keycode);
	button->set_text(key_name);
}

void InputItem::_input(const Ref<InputEvent> event) {
	Ref<InputEventKey> event_key = event;
	if (wait_for_input && event_key.is_valid() && event_key->is_pressed()) {
		keycode = event_key->get_scancode();
		if (keycode != 0) {
			String key_name = OS::get_singleton()->get_scancode_string(keycode);
			Godot::print(key_name);
			Godot::print(std::to_string(keycode).c_str());
			button->set_text(key_name);
			button->set_disabled(false);
			wait_for_input = false;
			_update_key_event(action, keycode);
		}
	}
}

void InputItem::_on_button_pressed() {
	button->set_text("Waiting for input...");
	button->set_disabled(true);
	button->release_focus();
	wait_for_input = true;
}

void InputItem::set_keycode(const int64_t code) {
	keycode = code;
}

void InputItem::set_action(const String action) {
	this->action = action;
}

int InputItem::get_keycode() {
	return button->get_text().to_int();
}

String InputItem::get_action() {
	return label->get_text();
}

void InputItem::_update_key_event(const String action, const int64_t keycode) {
	Godot::print("Update Key Event");
	SettingsManager *settingsManager = Object::cast_to<SettingsManager>(get_tree()->get_root()->get_node("SettingsManager"));
	settingsManager->update_key_event(action, keycode);
}

void InputItem::_register_methods() {
	register_method("_ready", &InputItem::_ready);
	register_method("_init", &InputItem::_init);
	register_method("_input", &InputItem::_input);
	register_method("set_action", &InputItem::set_action);
	register_method("get_action", &InputItem::get_action);
	register_method("_on_button_pressed", &InputItem::_on_button_pressed);
}
